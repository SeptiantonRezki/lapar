part of 'pages.dart';

class AdressPage extends StatefulWidget {
  final User user;
  final String password;
  final File pictureFile;

  AdressPage(this.user, this.password, this.pictureFile);

  @override
  _AdressPageState createState() => _AdressPageState();
}

class _AdressPageState extends State<AdressPage> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController houseNumController = TextEditingController();
  bool isLoading = false;

  List<String> cities;
  String selectedCity;

  @override
  void initState() {
    super.initState();
    cities = ['Bandung', 'Jakarta', 'Sumedang'];
    selectedCity = cities[0];
  }

  @override
  Widget build(BuildContext context) {
    Widget bgColor() {
      return Container(
        width: double.infinity,
        height: double.infinity,
        color: orangeColor.withOpacity(0.8),
      );
    }

    Widget logo() {
      return Container(
        width: 95,
        height: 95,
        margin: const EdgeInsets.only(top: 88, bottom: 13),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/logo.png'), fit: BoxFit.cover)),
      );
    }

    Widget title() {
      return Text(
        'Getting Started',
        style: blackFontStyle1.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      );
    }

    Widget subtitle() {
      return Text(
        'Create an account to continued',
        style: blackFontStyle1.copyWith(
          fontSize: 13,
          fontWeight: FontWeight.w400,
          color: Colors.white,
        ),
      );
    }

    Widget inputPhoneNum() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'Phone No',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: phoneController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: greyFontStyle,
                hintText: 'Type your phone number',
              ),
            ),
          ),
        ],
      );
    }

    Widget inputAddress() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'Address',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: addressController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: greyFontStyle,
                hintText: 'Type your address',
              ),
            ),
          ),
        ],
      );
    }

    Widget inputHouseNum() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'House No',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: houseNumController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: greyFontStyle,
                hintText: 'Type your house number',
              ),
            ),
          ),
        ],
      );
    }

    Widget inputCity() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'City',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: DropdownButton(
                value: selectedCity,
                isExpanded: true,
                underline: SizedBox(),
                onChanged: (item) {
                  setState(() {
                    selectedCity = item;
                  });
                },
                items: cities
                    .map((e) => DropdownMenuItem(
                        value: e,
                        child: Text(
                          e,
                          style: blackFontStyle3,
                        )))
                    .toList()),
          ),
        ],
      );
    }

    Widget signUpButton() {
      return Container(
        width: double.infinity,
        height: 45,
        margin: EdgeInsets.only(top: defaultMargin),
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: (isLoading == true)
            ? Center(child: loadingIndicator)
            : RaisedButton(
                onPressed: () async {
                  User user = widget.user.copyWith(
                    phoneNumber: phoneController.text,
                    address: addressController.text,
                    houseNumber: houseNumController.text,
                    city: selectedCity,
                  );

                  setState(() {
                    isLoading = true;
                  });

                  await context.bloc<UserCubit>().signUp(user, widget.password,
                      pictureFile: widget.pictureFile);
                  UserState state = context.bloc<UserCubit>().state;

                  if (state is UserLoaded) {
                    context.bloc<FoodCubit>().getFood();
                    context.bloc<TransactionCubit>().getTransactions();
                    Get.to(MainPage());
                  } else {
                    Get.snackbar(
                      '',
                      '',
                      backgroundColor: 'D9435E'.toColor(),
                      icon: Icon(
                        MdiIcons.closeCircleOutline,
                        color: Colors.white,
                      ),
                      titleText: Text(
                        'Sign In Failed',
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      messageText: Text(
                        (state as UserLoadingFailed).message,
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                        ),
                      ),
                    );
                    setState(() {
                      isLoading = false;
                    });
                  }
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
                color: mainColor,
                child: Text(
                  'Sign Up',
                  style: blackFontStyle3.copyWith(color: Colors.white),
                ),
              ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          bgColor(),
          Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  logo(),
                  title(),
                  const SizedBox(
                    height: 10,
                  ),
                  subtitle(),
                  const SizedBox(
                    height: 56,
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: 14),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 34),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(33),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        inputPhoneNum(),
                        inputAddress(),
                        inputHouseNum(),
                        inputCity(),
                        signUpButton(),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
