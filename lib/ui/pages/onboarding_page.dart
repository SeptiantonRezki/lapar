part of 'pages.dart';

class OnboardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget bgImage() {
      return Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/send.png'),
          ),
        ),
      );
    }

    Widget title() {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          margin: const EdgeInsets.only(
            top: 88,
          ),
          child: Text(
            'Browse & Order All Products\nat Any Time',
            style: blackFontStyle1.copyWith(
                fontSize: 20, fontWeight: FontWeight.w700, color: orangeColor),
            textAlign: TextAlign.center,
          ),
        ),
      );
    }

    Widget button() {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: const EdgeInsets.only(bottom: 88),
          width: 140,
          height: 42,
          decoration: BoxDecoration(
            color: orangeColor,
            borderRadius: BorderRadius.circular(18),
          ),
          child: TextButton(
            onPressed: () {
              Get.to(SignInPage());
            },
            child: Text('Get Started',
                style: blackFontStyle2.copyWith(
                    color: Colors.white, fontWeight: FontWeight.w300)),
          ),
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          bgImage(),
          title(),
          button(),
        ],
      ),
    );
  }
}
